colorscheme desert
filetype plugin indent on
syntax enable
set encoding=utf-8 ttyfast

set number relativenumber

set wildmenu wildmode=list:longest,full wildignorecase
set wildignore+=.git,*.o,*.swp

set viminfofile=${PREFIX}/var/cache/viminfo undodir=${TMPDIR}// undofile

set laststatus=2 showcmd showmode
set statusline=%F%m%r%h%w%=[%l/%L][%02v]\ %y[%{&fileformat}]

set autoread hidden lazyredraw splitbelow splitright

set nohlsearch incsearch ignorecase smartcase wrapscan

set expandtab smarttab shiftround autoindent

set shiftwidth=2 tabstop=4 softtabstop=4

set wrap linebreak breakindent

set backspace=indent,eol,start scrolloff=2

nmap <Space> <Leader>

nnoremap <Leader>e :split ${MYVIMRC}<CR>
nnoremap <Leader>h :set hlsearch!<CR>
nnoremap <Leader>l :set list!<CR>
nnoremap <Leader>n :set number! relativenumber!<CR>
nnoremap <Leader>r :source ${MYVIMRC}<CR>
nnoremap <Leader>s :setlocal spell!<CR>
nnoremap <Leader>w :set wrap!<CR>

nnoremap <Leader><Right> <C-]>
nnoremap <Leader><Left> <C-T>

nnoremap <Leader><CR> O<Esc>
nnoremap <CR> o<Esc>

nnoremap S :%s//g<Left><Left>
xnoremap S :s//g<Left><Left>

nnoremap <Up> gk
nnoremap <Down> gj
nnoremap <Home> g<Home>
nnoremap <End> g<End>
inoremap <Up> <C-o>gk
inoremap <Down> <C-o>gj
inoremap <Home> <C-o>g<Home>
inoremap <End> <C-o>g<End>

nnoremap Y y$

xnoremap x "_x
xnoremap X "_X

xnoremap < <gv
xnoremap > >gv

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

augroup MyVimrc
  autocmd!
  autocmd BufWritePre * %s/\s\+$//e
  autocmd FileType * setlocal formatoptions-=cro
augroup END
