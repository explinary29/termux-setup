if [[ "$-" != *i* ]]; then
  return
fi

umask 0022

shopt -s cdspell direxpand dirspell

bold="$(tput bold)"
reset="$(tput sgr0)"
c0="$(tput setaf 4)"
c1="$(tput setaf 2)"
c2="$(tput setab 4; tput setaf 7)"

PS1="\[${bold}\]\[${c0}\]\w ➜\[${reset}\] "

export LESS="-FgiMnR"
export LESSHISTFILE="${PREFIX}/var/cache/lesshst"
export LESS_TERMCAP_md="${c0}"          # begin bold
export LESS_TERMCAP_me="${reset}"       # reset bold
export LESS_TERMCAP_us="${c1}"          # begin underline
export LESS_TERMCAP_ue="${reset}"       # reset underline
export LESS_TERMCAP_so="${c2}"          # begin reverse video
export LESS_TERMCAP_se="${reset}"       # reset reverse video

unset bold reset c{0..2}

HISTCONTROL="ignoreboth:erasedups"
HISTFILESIZE="128"
HISTTIMEFORMAT="[%F %T]  "

alias ls="ls --color=auto --group-directories-first"

wttr() {
  local request="https://wttr.in/$1"
  if (( COLUMNS < 125 )); then
    request+="?n"
  fi
  curl --compressed "${request}"
}

wmip() {
  curl "https://ifconfig.me/$1" && printf "\n"
}

how-to() {
  local subject="$1" && shift
  IFS="+" curl "https://cht.sh/${subject}/$*"
}
